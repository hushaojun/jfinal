package org.gongliang.weixin.menu;

/**
 * 
 * @author Javen
 * 2016年5月30日
 */
public class ClickButton extends Button{
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
}
