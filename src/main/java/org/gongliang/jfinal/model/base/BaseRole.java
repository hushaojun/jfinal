package org.gongliang.jfinal.model.base;

import org.gongliang.jfinal.common.base.ModelExt;

import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseRole<M extends BaseRole<M>> extends ModelExt<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public M setRoleName(java.lang.String roleName) {
		set("role_name", roleName);
		return (M)this;
	}

	public java.lang.String getRoleName() {
		return get("role_name");
	}

	public M setRoleCode(java.lang.String roleCode) {
		set("role_code", roleCode);
		return (M)this;
	}

	public java.lang.String getRoleCode() {
		return get("role_code");
	}

	public M setStatus(java.lang.String status) {
		set("status", status);
		return (M)this;
	}

	public java.lang.String getStatus() {
		return get("status");
	}

	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}

	public java.lang.String getRemark() {
		return get("remark");
	}

	public M setCreateDate(java.util.Date createDate) {
		set("create_date", createDate);
		return (M)this;
	}

	public java.util.Date getCreateDate() {
		return get("create_date");
	}

	public M setUpdateDate(java.util.Date updateDate) {
		set("update_date", updateDate);
		return (M)this;
	}

	public java.util.Date getUpdateDate() {
		return get("update_date");
	}

	public M setCreator(java.lang.Integer creator) {
		set("creator", creator);
		return (M)this;
	}

	public java.lang.Integer getCreator() {
		return get("creator");
	}

	public M setUpdater(java.lang.Integer updater) {
		set("updater", updater);
		return (M)this;
	}

	public java.lang.Integer getUpdater() {
		return get("updater");
	}

}
