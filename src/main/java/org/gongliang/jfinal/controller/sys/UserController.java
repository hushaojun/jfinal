package org.gongliang.jfinal.controller.sys;

import java.util.List;

import org.gongliang.jfinal.common.KConstant;
import org.gongliang.jfinal.common.base.Conditions;
import org.gongliang.jfinal.common.base.Result;
import org.gongliang.jfinal.common.exception.ExceptionEnum;
import org.gongliang.jfinal.common.interceptor.GlobalInterceptor;
import org.gongliang.jfinal.common.interceptor.LoginValidate;
import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.Resources;
import org.gongliang.jfinal.model.User;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.ActionKey;
import com.jfinal.json.JFinalJson;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.render.JsonRender;

public class UserController extends BaseController<User>{

	public void main() {
		render("/views/sys/User/main.html");
	}

	public void rolePage() {
		setAttr("userId", getPara("userId"));
		render("/views/sys/User/role.html");
	}

	public void updatePage() {
		render("/views/sys/User/user.html");
	}

	@ActionKey("/getMenu")
   public void getUserPermission(){
		Result result = Result.success("查找用户权限成功!");
		User user=getUser();
		List<Record> menuLis=getSessionAttr("menuList");
		if(null==menuLis&&user!=null){
			menuLis=Resources.dao.findUserPermission(user.getId(),1);
			setSessionAttr("menuList", menuLis);
		}
		result.setRows(menuLis);
		render(new JsonRender(JFinalJson.getJson().toJson(result)).forIE());
	}

	@Before(LoginValidate.class)
	@Clear(GlobalInterceptor.class)
	@ActionKey("/sys/login")
	public void login() {
		Result result = Result.success("用户登录成功!");
		User user=getUser();
    	if(user==null){
        	user=User.dao.login(getPara("username"), getPara("password"));
			if (user == null) {
				result.setExceptionEnum(ExceptionEnum.USER_PWD_ERROR);
			} else {
				setSessionAttr(KConstant.USER_SESSION, user);
        	}
    	}
		render(new JsonRender(result).forIE());
	}
	@ActionKey("/index")
	public void index(){
		User user=getUser();
		if(user==null){
			setAttr("msg", "session过期!");
			render("/views/login.html");
		}else{
			render("/views/index.html");
		}
	}

	@ActionKey("/sys/logout")
	public void logout(){
		getSession().removeAttribute("loginUser");
		getSession().removeAttribute("menuList");
		getSession().invalidate();
		render("/views/login.html");
	}

	@ActionKey("/sys/updatePwdPage")
	public void updatePwdPage() {
		render("/views/password.html");
	}

	public void updatePassword(){
		User user=getUser();
		Result result = Result.success("密码修改成功!");
		if(StrKit.isBlank(getPara("newPassword"))){
			render(new JsonRender("新密码不能为空!").forIE());
			return;
		}
		user.set("password", HashKit.md5(HashKit.sha1(user.getUsername()+getPara("newPassword"))));
		if(!user.update()){
			result = Result.error("密码修改失败!");
			render(new JsonRender(result).forIE());
		}else{
			render(new JsonRender(result).forIE());
		}
		
	}

	@Override
	public String getSql() {
		Conditions condi = new Conditions();
		User user=new User();
		// user.setEmail(getPara("name"));
		user.setUsername(getPara("username"));
		user.setName(getPara("name"));
		user.setStatus(getPara("status"));
		// user.setPhone(getPara("name"));
		user.setCreateDate(null);
    	condi.setValueQuery(Conditions.GREATER_EQUAL, "inputTime", getPara("startTime")); //开始时间
    	condi.setValueQuery(Conditions.LESS_EQUAL, "inputTime", getPara("endTime")); //结束时间
		condi.setFiledQuery(Conditions.FUZZY, "phone", "name", "username", "email");
    	condi.modelToCondition(user,"u");
    	params.clear();
    	params.addAll(condi.getParamList());
    	String sql="select u.*,d.dept_name from sys_user u left join sys_department d on u.dept_id=d.id  ";
		return sql+condi.getSql();
	}
}
