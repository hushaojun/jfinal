package org.gongliang.jfinal.controller.sys;

import java.util.List;

import org.gongliang.jfinal.common.base.Result;
import org.gongliang.jfinal.common.exception.ExceptionEnum;
import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.Resources;

import com.google.common.collect.Lists;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.render.JsonRender;

public class ResourcesController extends BaseController<Resources>{
	
	public void main() {
		render("/views/sys/Resources/main.html");
	}

	public void updatePage() {
		render("/views/sys/Resources/res.html");
	}

	public void saveBasePermission(){
		Result result = Result.success(ExceptionEnum.QUERY_SUCCESS);
		int parentId=getParaToInt("parentId");
		int level=getParaToInt("level");
		List<Resources> lists=Lists.newArrayList();
		Resources save=new Resources("添加","create","icon-add",parentId,level,1);
		Resources update=new Resources("修改","update","icon-edit",parentId,level,2);
		Resources delete=new Resources("删除","delete","icon-remove",parentId,level,3);
		Resources list=new Resources("查询","list","icon-search",parentId,level,4);
		lists.add(save);
		lists.add(update);
		lists.add(delete);
		lists.add(list);
		Db.batchSave(lists, 10);
		render(new JsonRender(result).forIE());
	}


	@Override
	public String getSql() {
		String sql = "select * from " + tablename;
		return sql;
	}
}
