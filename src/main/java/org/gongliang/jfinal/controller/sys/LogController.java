package org.gongliang.jfinal.controller.sys;

import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.Log;

public class LogController extends BaseController<Log>{
	
	public void main() {
		render("/views/sys/Log/main.html");
	}

	@Override
	public String getSql() {
		String sql = "select * from " + tablename;
		return sql;
	}
}
