package org.gongliang.jfinal.controller.sys;

import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.Department;

public class DepartmentController extends BaseController<Department>{

	public void main() {
		render("/views/sys/Department/main.html");
	}

	public void updatePage() {
		render("/views/sys/Department/dept.html");
	}

	@Override
	public String getSql() {
		String sql = "select * from " + tablename;
		return sql;
	}
}
