package org.gongliang.jfinal.controller.sys;

import org.apache.commons.lang3.StringUtils;
import org.gongliang.jfinal.common.base.Conditions;
import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.Dictionary;

import com.jfinal.kit.StrKit;

public class DictionaryController extends BaseController<Dictionary>{
	
	public void main() {
		render("/views/sys/Dictionary/main.html");
	}

	public void updatePage() {
		render("/views/sys/Dictionary/dict.html");
	}

	@Override
	public String getSql() {
		Conditions condi = new Conditions();
		Dictionary dict = new Dictionary();
		dict.setStatus(getPara("status", "1"));
		dict.setType(getPara("type"));
		condi.setFiledQuery(Conditions.EQUAL, "status");
		condi.setFiledQuery(Conditions.FUZZY_RIGHT, "type");
		condi.modelToCondition(dict, "d");
		params.clear();
		params.addAll(condi.getParamList());
		String sql = "select d.* from sys_dictionary d " + condi.getSql();
		if ("0".equals(getPara("parentId"))) {
			if (StringUtils.containsIgnoreCase(sql, "where")) {
				sql += "AND d.parent_id is null";
			}
		} else if (StrKit.notBlank(getPara("parentId")) && !"0".equals(getPara("parentId"))) {
			params.add(getPara("parentId"));
			if (StringUtils.containsIgnoreCase(sql, "where")) {
				sql += "AND d.parent_id=?";
			}
		}
		return sql;
	}
}
