package org.gongliang.jfinal.controller.sys;

import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.App;

public class AppController extends BaseController<App> {

	public void main() {
		render("/views/sys/App/main.html");
	}

	public void updatePage() {
		render("/views/sys/App/app.html");
	}


	@Override
	public String getSql() {
		String sql = "select * from " + tablename;
		return sql;
	}
}
