package org.gongliang.jfinal.controller.sys;

import org.gongliang.jfinal.controller.BaseController;
import org.gongliang.jfinal.model.Area;

public class AreaController extends BaseController<Area>{

	public void main() {
		render("/views/sys/Area/main.html");
	}

	@Override
	public String getSql() {
		Integer parentId = getParaToInt("parentId");
		String sql = "select * from " + tablename;
		if (parentId == null) {
			sql += " where parent_id is null";
		} else if (parentId != null && -1 == parentId) {
			sql += " where parent_id=0 OR level=2";
		} else if (parentId != null && -1 != parentId) {
			sql += " where parent_id=" + parentId;
		}
		return sql;
	}

}
