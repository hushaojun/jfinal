package org.gongliang.jfinal.common.exception;

/**
 * 
 * @Description 参数不合法异常
 * @author 龚亮
 * @date 2017年3月30日 下午3:34:16
 */
public class ParamException extends BaseException {

	private static final long serialVersionUID = 5996594069536988033L;

	/** @Fields paramName: 参数名称 */
    private String paramName;

    /** @Fields paramValue: 参数值 */
    private String paramValue;

	public ParamException(ExceptionEnum businessExceptionEnum) {
		super(businessExceptionEnum);
    }

	public ParamException(ExceptionEnum businessExceptionEnum,
			String paramName, String paramValue) {
		super(businessExceptionEnum);
        this.paramName = paramName;
        this.paramValue = paramValue;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
}
