package org.gongliang.jfinal.common.exception;

/**
 * 
 * @Description 基础异常
 * @author 龚亮
 * @date 2017年3月30日 下午3:34:16
 */
public abstract class BaseException extends RuntimeException {

	private static final long serialVersionUID = -1324022948441263568L;

	/** @Fields errCode: 内部定义错误码 */
	private Integer errCode;

	/** @Fields errMsg: 错误信息 */
	private String errMsg;

	public BaseException(ExceptionEnum businessExceptionEnum) {
		super();
		this.errCode = businessExceptionEnum.getResCode();
		this.errMsg = businessExceptionEnum.getResMsg();
	}

	public Integer getErrCode() {
		return errCode;
	}

	public void setErrCode(Integer errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
