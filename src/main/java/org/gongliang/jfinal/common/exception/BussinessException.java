package org.gongliang.jfinal.common.exception;

/**
 * 
 * @Description 通用逻辑异常
 * @author 龚亮
 * @date 2017年3月30日 下午3:34:16
 */
public class BussinessException extends BaseException {

	private static final long serialVersionUID = 5996594069536988033L;

	public BussinessException(ExceptionEnum businessExceptionEnum) {
		super(businessExceptionEnum);
    }
}
