package org.gongliang.jfinal.common.interceptor;

import org.gongliang.jfinal.common.base.KaptchaRender;
import org.gongliang.jfinal.common.base.Result;
import org.gongliang.jfinal.common.exception.ExceptionEnum;

import com.jfinal.core.Controller;
import com.jfinal.render.JsonRender;
import com.jfinal.validate.Validator;

public class LoginValidate extends Validator{

	@Override
	protected void handleError(Controller c) {
		Result result = Result.success(ExceptionEnum.KAPTCHA_ERROR);
		c.keepPara("username");
		c.render(new JsonRender(result).forIE());
	}

	@Override
	protected void validate(Controller c) {
		validateRequired("username", "msg", "请输入您的登陆账号");
		
		validateRequired("password", "msg", "请输入您的密码");
		validateString("password", 3, 24, "msg", "请输入6~24位的密码");
		
		validateRequired("captcha", "msg", "请输入验证码");
		validateCaptcha("captcha", "msg", "验证码错误");
	}

	@Override
	protected void validateCaptcha(String field, String errorKey, String errorMessage) {
		if (!KaptchaRender.validate(getController(), field))
			addError(errorKey, errorMessage);
	}
}
