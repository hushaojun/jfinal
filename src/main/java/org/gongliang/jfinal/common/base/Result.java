package org.gongliang.jfinal.common.base;

import java.util.List;
import java.util.Map;

import org.gongliang.jfinal.common.exception.ExceptionEnum;
import org.gongliang.jfinal.utils.BeanUtil;

import com.google.common.collect.Maps;

/**
 * @description 返回前台的json数据
 * @author 龚亮
 * @date 2015-05-18 14:42:28
 */
public class Result implements java.io.Serializable {

	private static final long serialVersionUID = 1L;


	public static final String STATE_SUCCESS = "ok";
	public static final String STATE_ERROR = "error";


	public static Result success(ExceptionEnum businessExceptionEnum) {
		return new Result(businessExceptionEnum, Result.STATE_SUCCESS);
	}

	public static Result error(ExceptionEnum businessExceptionEnum) {
		return new Result(businessExceptionEnum, Result.STATE_ERROR);
	}

	public static Result success() {
		return new Result();
	}

	public static Result error() {
		return new Result(ExceptionEnum.SYS_ERROR, Result.STATE_ERROR);
	}

	public static Result success(String resMsg) {
		return new Result(ExceptionEnum.SYS_SUCCESS.getResCode(), resMsg, Result.STATE_SUCCESS);
	}

	public static Result error(String resMsg) {
		return new Result(ExceptionEnum.SYS_ERROR.getResCode(), resMsg, Result.STATE_ERROR);
	}
	/**
	 * ok: 返回成功 error： 返回失败
	 */
	private String resState;
	/**
	 * 错误信息
	 */
	private String resMsg;
	/**
	 * 错误码
	 */
	private Integer resCode;
	/*
	 * 返回的结果格式 {total:0L,rows[]}
	 */
	private Map<String, Object> resData = Maps.newHashMap();
	/*
	 * 附带额外的参数
	 */
	private Map<String, Object> resExtra = Maps.newHashMap();


	public Result() {
		this.resState = Result.STATE_SUCCESS;
		this.resCode = ExceptionEnum.SYS_SUCCESS.getResCode();
		this.resMsg = ExceptionEnum.SYS_SUCCESS.getResMsg();
	}

	public Result(ExceptionEnum businessExceptionEnum,
			String resState) {
		this.resCode = businessExceptionEnum.getResCode();
		this.resMsg = businessExceptionEnum.getResMsg();
		this.resState = resState;
	}

	public Result(int resCode, String resMsg, String resState) {
		this.resCode = resCode;
		this.resMsg = resMsg;
		this.resState = resState;
	}


	public void setResData(List<?> rows, Object total) {
		resData.put("total", total);
		resData.put("rows", rows);
	}

	public void setResData(List<?> rows) {
		resData.put("total", rows.size());
		resData.put("rows", rows);
	}

	public void setTotal(Long total) {
		resData.put("total", total);
	}

	public void setRows(List<?> rows) {
		resData.put("rows", rows);
	}

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	public Integer getResCode() {
		return resCode;
	}

	public void setResCode(Integer resCode) {
		this.resCode = resCode;
	}

	public Map<String, Object> getResExtra() {
		return resExtra;
	}

	public void setResExtra(Map<String, Object> resExtra) {
		this.resExtra = resExtra;
	}

	public void setResExtraObj(Object resExtra) {
		this.resExtra = BeanUtil.beanToMap(resExtra);
	}

	public void putExtra(String key, Object value) {
		resExtra.put(key, value);
	}

	public Map<String, Object> getResData() {
		return resData;
	}

	public String getResState() {
		return resState;
	}

	public void setResState(String resState) {
		this.resState = resState;
	}

	public void setExceptionEnum(ExceptionEnum e) {
		this.resCode = e.getResCode();
		this.resMsg = e.getResMsg();
	}
}
