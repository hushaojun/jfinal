package org.gongliang.jfinal.common.base;

import java.io.File;

import org.gongliang.jfinal.utils.StringUtil;

import com.oreilly.servlet.multipart.FileRenamePolicy;


public class RandomFileRenamePolicy implements FileRenamePolicy{

	@Override
	public File rename(File file) {
		File dest=new File(file.getParentFile().getAbsolutePath()+"\\"+StringUtil.getTimeFileName()+"$"+file.getName());
        File dir = dest.getParentFile();
        if (!dir.exists()) {
            dir.mkdirs();
        }
		return dest;
	}
}
