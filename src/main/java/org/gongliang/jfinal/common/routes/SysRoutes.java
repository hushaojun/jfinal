package org.gongliang.jfinal.common.routes;

import org.gongliang.jfinal.controller.sys.AppController;
import org.gongliang.jfinal.controller.sys.AreaController;
import org.gongliang.jfinal.controller.sys.DepartmentController;
import org.gongliang.jfinal.controller.sys.DictionaryController;
import org.gongliang.jfinal.controller.sys.IndexController;
import org.gongliang.jfinal.controller.sys.LogController;
import org.gongliang.jfinal.controller.sys.ResourcesController;
import org.gongliang.jfinal.controller.sys.RoleController;
import org.gongliang.jfinal.controller.sys.UserController;

import com.jfinal.config.Routes;

public class SysRoutes extends Routes {

	@Override
	public void config() {
		add("/sys_user", UserController.class);
		add("/sys_app", AppController.class);
		add("/sys_area", AreaController.class);
		add("/sys_dept", DepartmentController.class);
		add("/sys_dictionary", DictionaryController.class);
		add("/sys_log", LogController.class);
		add("/sys_resources", ResourcesController.class);
		add("/sys_role", RoleController.class);
		add("/", IndexController.class);
	}

}
