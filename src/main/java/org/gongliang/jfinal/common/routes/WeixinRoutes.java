package org.gongliang.jfinal.common.routes;

import org.gongliang.weixin.controller.core.AllPayController;
import org.gongliang.weixin.controller.core.JSSDKController;
import org.gongliang.weixin.controller.wx.RedPackApiController;
import org.gongliang.weixin.controller.wx.WeiXinOauthController;
import org.gongliang.weixin.controller.wx.WeixinApiController;
import org.gongliang.weixin.controller.wx.WeixinMsgController;
import org.gongliang.weixin.controller.wx.WeixinPayController;
import org.gongliang.weixin.controller.wx.WeixinTransfersController;

import com.jfinal.config.Routes;

public class WeixinRoutes extends Routes {

	@Override
	public void config() {
		add("/wx_read", RedPackApiController.class);
		add("/wx_transfers", WeixinTransfersController.class);
		add("/wx_allpay", AllPayController.class);
		add("/wx_msg", WeixinMsgController.class);
		add("/wx_api", WeixinApiController.class);
		add("/wx_oauth", WeiXinOauthController.class);
		add("/wx_jssdk", JSSDKController.class);
		// 可以去掉 /front
		add("/wx_pay", WeixinPayController.class);
	}

}
