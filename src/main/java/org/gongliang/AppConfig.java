package org.gongliang;

import org.gongliang.jfinal.common.base.RandomFileRenamePolicy;
import org.gongliang.jfinal.common.interceptor.GlobalInterceptor;
import org.gongliang.jfinal.common.routes.SysRoutes;
import org.gongliang.jfinal.common.routes.WeixinRoutes;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.jfinal.upload.OreillyCos;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.api.ApiConfigKit;

public class AppConfig extends JFinalConfig {

	/**
	 * 如果要支持多公众账号，只需要在此返回各个公众号对应的 ApiConfig 对象即可 可以通过在请求 url 中挂参数来动态从数据库中获取
	 * ApiConfig 属性值
	 */
	public ApiConfig getApiConfig() {
		ApiConfig ac = new ApiConfig();

		// 配置微信 API 相关常量
		ac.setToken(PropKit.get("token"));
		ac.setAppId(PropKit.get("appId"));
		ac.setAppSecret(PropKit.get("appSecret"));

		/**
		 * 是否对消息进行加密，对应于微信平台的消息加解密方式： 1：true进行加密且必须配置 encodingAesKey
		 * 2：false采用明文模式，同时也支持混合模式
		 */
		ac.setEncryptMessage(PropKit.getBoolean("encryptMessage", false));
		ac.setEncodingAesKey(PropKit.get("encodingAesKey", "setting it in config file"));
		return ac;
	}

	/**
	 * 配置常量
	 */
	@Override
	public void configConstant(Constants me) {
		PropKit.use("jdbc.properties");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setUrlParaSeparator("-");
		me.setViewType(ViewType.JFINAL_TEMPLATE);
		me.setBaseUploadPath(PropKit.get("fileUploadPath"));
		me.setError500View("/public/error/500.html");
		me.setError404View("/public/error/404.html");
		me.setJsonFactory(new FastJsonFactory());
		me.setJsonDatePattern("yyyy-MM-dd HH:mm:ss");
		ApiConfigKit.setDevMode(me.getDevMode());
	}

	public static C3p0Plugin createC3p0Plugin() {
		return new C3p0Plugin(PropKit.get("url"), PropKit.get("username"), PropKit.get("password").trim(),
				PropKit.get("driver"));
	}

	public static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(PropKit.get("url"), PropKit.get("username"), PropKit.get("password").trim(),
				PropKit.get("driver"));
	}

	/**
	 * 配置路由
	 */
	@Override
	public void configRoute(Routes me) {
		me.add(new SysRoutes());
		me.add(new WeixinRoutes());
	}

	/**
	 * 配置插件
	 */
	@Override
	public void configPlugin(Plugins me) {

		DruidPlugin druidPlugin = createDruidPlugin();
		druidPlugin.addFilter(new StatFilter());
		WallFilter wall = new WallFilter();
		wall.setDbType("mysql");
		druidPlugin.addFilter(wall);
		me.add(druidPlugin);
		// C3p0Plugin c3p0Plugin = createC3p0Plugin();
		// me.add(c3p0Plugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setShowSql(PropKit.getBoolean("devMode", false));
		// arp.setBaseSqlTemplatePath(PathKit.getRootClassPath());
		// arp.addSqlTemplate("sys.sql");
		me.add(arp);

		Cron4jPlugin cp = new Cron4jPlugin("job.properties");
		me.add(cp);

		// 所有配置在 MappingKit 中搞定
		_MappingKit.mapping(arp);

		// ehcahce插件配置
		me.add(new EhCachePlugin());
	}

	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new GlobalInterceptor());
		me.add(new SessionInViewInterceptor());
	}

	/**
	 * 配置处理器
	 */
	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("ctx"));
	}

	@Override
	public void afterJFinalStart() {
		super.afterJFinalStart();
		ApiConfigKit.putApiConfig(getApiConfig());
		OreillyCos.setFileRenamePolicy(new RandomFileRenamePolicy());
	}

	public static void main(String[] args) {
		// JFinal.start("WebContent", 9090, "/jfinal", 5);
		JFinal.start("src/main/webapp", 9090, "/jfinal", 5);
	}

	@Override
	public void configEngine(Engine me) {
		me.setBaseTemplatePath(PathKit.getWebRootPath());
		// 共享对象
		me.addSharedObject("ctx", JFinal.me().getContextPath());
	}

}
