// 判断时候在Iframe框架内,在则刷新父页面
if (self != top) {
    parent.location.reload(true);
    if (!!(window.attachEvent && !window.opera)) {
        document.execCommand("stop");
    } else {
        window.stop();
    }
}

$(function () {
    // 验证码
    $("#img_captcha").click(function() {
        var $this = $(this);
        var url = $this.attr("src") +"?t="+ new Date().getTime();
        $this.attr("src", url);
    });
    // 登录
    $('#loginform').form({
        url:JSmile.baseUrl+'/sys/login',
        onSubmit : function() {
            var isValid = $(this).form('validate');
            if(!isValid){
    	    	var isValid = $(this).form('validate');
    			return isValid;	// 返回false终止表单提交
            }
            return isValid;
        },
        success:function(result){
        	console.log(result);
            result = $.parseJSON(result);
            if ('ok'==result.resState) {
                window.location.href =JSmile.baseUrl+"/index";
            }else{
            	$("#msg").html(result.resMsg);
                // 刷新验证码
                $("#img_captcha")[0].click();
            }
        }
    });
});
function submitForm(){
    $('#loginform').submit();
}
function clearForm(){
    $('#loginform').form('clear');
}
//回车登录
function enterlogin(){
    if (event.keyCode == 13){
        event.returnValue=false;
        event.cancel = true;
        $('#loginform').submit();
    }
}
