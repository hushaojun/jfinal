$(function(){
	$('#menu_tree').tree({
		lines:true,
        url:JSmile.baseUrl+"/getMenu",
        loadFilter:function(data,parent){
        	return JSmile.convert(data,{parentField:"parent_id"});
        },
        onClick : function(node) {
        	var _this=this;
            var opts = {
                    title : node.text,
                    border : false,
                    closable : true,
                    fit : true,
                    iconCls : node.iconCls
                };
                var url = JSmile.baseUrl+node.url;
                if($(_this).tree('isLeaf', node.target)){
                    if (!(node.openMode == 'iframe')) {
                        opts.content = '<iframe src="' + url + '" frameborder="0" scrolling="auto" style="border:0;width:100%;height:99.5%;"></iframe>';
                        addTab(opts);
                    } else if (url) {
                        opts.href = url;
                        addTab(opts);
                    }
                }
        }
	});
	/**********修改密码*************/
	$("#btnUserPwd").click(
		function(){
 	        $.modalDialog({
                _form:"#upd_form", 
                submitSuccess:function(res){
                	window.location.href = JSmile.baseUrl+'/sys/logout';
                },
 	            title : '用户密码修改',
	            iconCls:'sn-lock',
	            width : 400,
	            height : 300,
 	            href:"/sys/updatePwdPage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#userForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
		}
	);
	/**********退出系统*************/
	$("#btnExit").click(
		function(){
			$.messager.confirm("后台管理系统","确认要退出后台管理系统平台？",function(r){
				if(r)
					window.location.href = JSmile.baseUrl+'/sys/logout';
			});
		}
	);
	/**********跳转至主页*************/
	$("#mainTabs_jumpHome").click(
		function(){
			$('#mainTabs').tabs('select',0);
		}
	);
	/**********刷新tab*************/
	$("#mainTabs_refTab").click(
		function(){
			var tab = $('#mainTabs').tabs('getSelected');  
			$('#mainTabs').tabs('update', {
				tab: tab,
				options: {
					title: tab.panel('options').title,
					content: tab.panel('options').content  
				}
			});
		}
	);
	/**********关闭全部tab*************/
	$("#mainTabs_closeTab").click(
		function(){
			var allTabs = $("#mainTabs").tabs('tabs');
		    for(var i = 0, len = allTabs.length; i < len; i++) {
		      $("#mainTabs").tabs('close', 1);
		    }
		}
	);
	/**********全屏切换*************/
	$("#btnFullScreen").click(
	   function(){
		   if (fullScreenApi.supportsFullScreen) {
			   if(!fullScreenApi.isFullScreen()){
				   fullScreenApi.requestFullScreen(document.documentElement);
			   }else{
				   fullScreenApi.cancelFullScreen(document.documentElement);
			   }
			}else{
				alert("您的破浏览器不支持全屏API哦，请换高版本的chrome或者firebox！");
			}
	   }
	);
});
function addTab(opts) {
    var t = $('#mainTabs');
    if (t.tabs('exists', opts.title)) {
        t.tabs('select', opts.title);
    } else {
        t.tabs('add', opts);
    }
}