/*
 * 注意  zIndex改为9999
 * accept  mimeTypes: 'image/gif,image/jpeg,image/png,image/jpg,image/bmp'
 */
UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
UE.Editor.prototype.getActionUrl = function(action) {
	if(action == 'uploadimage'|| action == 'uploadscrawl'){
		return JSmile.baseUrl+"/ueditor/uploadImage";
	}else if(action == 'uploadfile'){
		return JSmile.baseUrl+"/ueditor/uploadFile";
	}else if(action == 'uploadvideo'){
		return JSmile.baseUrl+"/ueditor/uploadVideo";
	}else if(action == 'listimage'){
		return JSmile.baseUrl+"/ueditor/listImage";
	}else if(action == 'listfile'){
		return JSmile.baseUrl+"/ueditor/listFile";
	}else {
		return this._bkGetActionUrl.call(this, action);
	}
}