$(function(){
  var resources={
     init:function(){
    	 var _this=this;
    	 _this.initTreegrid();
    	 _this.initEvent();
     },
     initEvent:function(){
    	var _this=this;
		$("#addPermission").click(function(){
			var selRows=_this.resourcesTreeGrid.treegrid('getSelections');
			if(selRows.length==0){
				$.messager.alert("提示", "请选择其中的一行！", "info");  
				return;
			}else{
				var level=selRows[0].level;
				if(level<2){
					$.messager.alert("提示", "只有二级菜单才能添加基本权限!", "info");
				}else{
	                JSmile.KHttp.send({url:"/sys_resources/saveBasePermission",data:{"parentId":selRows[0].id,"level":selRows[0].level+1}}).done(function(res){
						_this.resourcesTreeGrid.treegrid('clearSelections').treegrid('reload');
	                });
				}
			}
		});
 		//添加实现
 		$("#add_resources").click(function(){
 	        top.$.modalDialog({
                _form:"#resourcesForm", 
                _treegrid:_this.resourcesTreeGrid,
 	            title : '资源添加',
 	            href:"/sys_resources/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#resourcesForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
 		});
 		//删除实现
 		$("#del_resources").click(function(){
 			var selRows=_this.resourcesTreeGrid.treegrid('getSelections');
 			JSmile.KEasy.del('/sys_resources/deleteByIds',selRows,function(res){
 				_this.resourcesTreeGrid.treegrid('clearChecked').treegrid('reload');
 			});
 		});
 		$("#edit_resources").click(function(){
 			var selRows=_this.resourcesTreeGrid.treegrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#resourcesForm", 
 	                _treegrid:_this.resourcesTreeGrid, 	 	        	
 	 	            title : '资源信息修改',
 	 	            href:"/sys_resources/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#resourcesForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            }
 	 	    }).dialog('open');
 			});
 		});
 		//搜索实现
 		$("#search_resources").click(function(){
 			_this.resourcesTreeGrid.treegrid('load',$('#resourcesSearchForm').serializeJson());
 		}); 
     },
     initTreegrid:function(){//初始化各种treegrid
    	var _this=this;
    	_this.resourcesTreeGrid=$('#resources_treegrid').treegrid(JSmile.KEasy.initTreegrid({
	    	url:'/sys_resources/findAll',
	    	toolbar:'#resources_toolbar',
	    	loadFilter:function(data,parentId){
	    		var d={};
	    		d.rows=JSmile.convertTreegrid(data);
	    		d.total=data.resData.total;
	    		return d;
	    	},
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'name',title:'资源名称',align:'left',width:100},
			 {field:'url',title:'资源地址',align:'center',width:100},
			 {field:'permCode',title:'权限编码',align:'center',width:100},
			 {field:'level',title:'级数',align:'center',width:80},
			 {field:'sort',title:'排序',align:'center',width:80},
			 {field:'type',title:'类型',align:'center',width:80,formatter:function(value,row,index){
				 if(1==value)return "菜单";else if(2==value)return "权限";
			 }},
			 {field:'createDate',title:'时间',align:'center',width:120,sortable:true},
			 {field:'remark',title:'备注',align:'center',width:120},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:JSmile.KStatus.initStatus}
		]]
	    }));
     }
  };
  resources.init();
});