$(function(){
  var app={
     init:function(){
    	 var _this=this;
    	 _this.initData();
    	 _this.initDatagrid();
    	 _this.initEvent();
     },
     initData:function(){
		$('#type').combobox({
		    url:JSmile.baseUrl+'/sys_dictionary/findAll?type=app.category.',
		    valueField:'value',
		    textField:'label',
		    width:150,
		    editable:false
		});
     },
     initEvent:function(){
    	 var _this=this;
 		//添加实现
 		$("#add_app").click(function(){
 	        top.$.modalDialog({
                _form:"#appForm", 
                _datagrid:_this.appDataGrid,
 	            title : '添加app',
 	            href:"/sys_app/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#appForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
 		});
 		//删除实现
 		$("#del_app").click(function(){
 			var selRows=_this.appDataGrid.datagrid('getSelections');
 			JSmile.KEasy.del('/sys_app/deleteByIds',selRows,function(res){
 				_this.appDataGrid.datagrid('clearChecked').datagrid('reload');
 			});
 		});
 		$("#edit_app").click(function(){
 			var selRows=_this.appDataGrid.datagrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#appForm", 
 	                _datagrid:_this.appDataGrid, 	 	        	
 	 	            title : 'app修改',
 	 	            href:"/sys_app/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#appForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            }
 	 	    }).dialog('open');
 			});
 		});
 		//搜索实现
 		$("#search_app").click(function(){
 			_this.appDataGrid.datagrid('load',$('#appSearchForm').serializeJson());
 		}); 
     },
     initDatagrid:function(){//初始化各种datagrid
    	var _this=this;
    	_this.appDataGrid=$('#app_datagrid').datagrid(JSmile.KEasy.initDatagrid({
	    	url:'/sys_app/findAllByPage',
	    	toolbar:'#app_toolbar',
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'ck',checkbox:true},
			 {field:'versionName',title:'版本号',align:'center',width:100,sortable:true},
			 {field:'versionCode',title:'自然数版本号',align:'center',width:100,sortable:true},
			 {field:'appUrl',title:'地址',align:'center',width:120},
			 {field:'type',title:'类型',align:'center',width:80,formatter:function(value,row,index){
				 if(1==value)return "安卓";else if(2==value)return "苹果";else return "其它";
			 }},
			 {field:'isImposed',title:'是否强制升级',align:'center',width:80,sortable:true,formatter:function(value,row,index){
				 if(1==value)
					return "<div style=\"background-color:#df1717;width:60%;color:white;margin:0 auto;\">是</div>";
				 else
					return "<div style=\"background-color:#bf3e7e;width:60%;color:white;margin:0 auto;\">否</div>";
			 }},
			 {field:'createDate',title:'时间',align:'center',width:120,sortable:true},
			 {field:'remark',title:'备注',align:'center',width:120},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:JSmile.KStatus.initStatus},
			 {field:'action',title:'操作',align:'center',width:100,formatter: function(value,row,index){
				 if(row.status=='1'){
					 return JSmile.Kit.formatString('<a style="cursor:pointer;" class="easyui-tooltip" title=\"禁用\" onclick="updateRoleStatus(\'{0}\',\'{1}\');" ><i style="color:#5cb85c !important" class="iconfont sn-disable"></i></a>', row.id,0);
				 }else{
					 return JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;" title=\"启用\" onclick="updateRoleStatus(\'{0}\',\'{1}\');" ><i style="color:#d9534f !important" class="iconfont sn-enable"></i></a>', row.id,1);
				 }
			 }}
		]]
	    }));
     }
  };
  app.init();
});
function updateRoleStatus(id,status){
	JSmile.KHttp.send({
		url:"/sys_app/saveORupdate",
		data:{"id":id,"status":status}
	}).done(function(){
		$('#app_datagrid').datagrid('reload');
	});
}