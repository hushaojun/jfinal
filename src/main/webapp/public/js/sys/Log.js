$(function(){
  var log={
     init:function(){
    	 var _this=this;
    	 _this.initDatagrid();
    	 _this.initEvent();
     },
     initEvent:function(){
    	var _this=this;
 		//删除实现
 		$("#del_log").click(function(){
 			var selRows=_this.logDataGrid.datagrid('getSelections');
 			JSmile.KEasy.del('/sys_log/deleteByIds',selRows,function(res){
 				_this.logDataGrid.datagrid('clearChecked').datagrid('reload');
 			});
 		});
 		//搜索实现
 		$("#search_log").click(function(){
 			_this.logDataGrid.datagrid('load',$('#logSearchForm').serializeJson());
 		}); 
     },
     initDatagrid:function(){//初始化各种datagrid
    	var _this=this;
    	_this.logDataGrid=$('#log_datagrid').datagrid(JSmile.KEasy.initDatagrid({
	    	url:'/sys_log/findAllByPage',
	    	toolbar:'#log_toolbar',
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'ck',checkbox:true},
			 {field:'ip',title:'ip地址',align:'center',width:80},
			 {field:'username',title:'用户名',align:'center',width:80},
			 {field:'userAgent',title:'客户端信息',align:'center',width:100},
			 {field:'reqUrl',title:'请求地址',align:'center',width:100},
			 {field:'reqParam',title:'请求参数',align:'center',width:100},
			 {field:'resParam',title:'返回值',align:'center',width:100},
			 {field:'timeConsuming',title:'耗时',align:'center',width:80},
			 {field:'exception',title:'异常信息',align:'center',width:100},
			 {field:'reqOk',title:'是否成功',align:'center',width:80,sortable:true,formatter:function(value,row,index){
				 if(1==value)
					return "<div style=\"background-color:#5cb85c;width:60%;color:white;margin:0 auto;\">成功</div>";
				 else
					return "<div style=\"background-color:#d9534f;width:60%;color:white;margin:0 auto;\">失败</div>";
			 }},
			 {field:'createDate',title:'时间',align:'center',width:100,sortable:true},
			 {field:'remark',title:'备注',align:'center',width:100}
	    	]]
	    }));
     }
  };
  log.init();
});