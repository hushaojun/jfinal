$(function(){
  var area={
     init:function(){
    	 var _this=this;
    	 _this.initTreegrid();
     },
     initTreegrid:function(){//初始化各种datagrid
    	$('#area_treegrid').treegrid(JSmile.KEasy.initTreegrid({
	    	url:'/sys_area/findAll?level=3',
	    	treeField:"areaName",
	    	loadFilter:function(data,parentId){
	    		var d={};
	    		d.rows=JSmile.convertTreegrid(data,{textField:"areaName"});
	    		d.total=data.resData.total;
	    		return d;
	    	},
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'areaName',title:'区域名称',align:'left',width:120},
			 {field:'areaCode',title:'区域编码',align:'center',width:100},
			 {field:'shortName',title:'区域简称',align:'center',width:100},
			 {field:'lng',title:'经度',align:'center',width:100},
			 {field:'lat',title:'纬度',align:'center',width:100},
			 {field:'sort',title:'排序',align:'center',width:80},
			 {field:'level',title:'级数',align:'center',width:80},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:function(value,row,index){
				  if(value==1||value=='1'){
						return "<div style=\"background-color:#5cb85c;width:60%;color:white;margin:0 auto;\">正常</div>";
					}else{
						return "<div style=\"background-color:#d9534f;width:60%;color:white;margin:0 auto;\">禁用</div>";
					}
			 }}
	    	]]
	    }));
     }
  };
  area.init();
});