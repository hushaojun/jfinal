$(function(){
  var user={
     init:function(){
    	 var _this=this;
    	 _this.initDatagrid();
    	 _this.initEvent();
     },
     initEvent:function(){
	    var _this=this;
		//用户角色实现
		$("#user_role").click(function(){
 			var selRows=_this.userDataGrid.datagrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
     	        top.$.modalDialog({
                    width:300,
                    height:400,
     	            title : '用户角色修改',
     	            href:"/sys_user/rolePage?userId="+selRows[0].id,
            		buttons:[{text:'确定',handler:function(){
        			   var role_tree=top.$.modalDialog.handler.find("#role_tree"); 
        	           var sels=role_tree.tree('getChecked');
        	           var roleIds=[];
        	           for(var i=0;i<sels.length;i++){
        	        	   roleIds.push(sels[i].id);
        	           }
	     	           	JSmile.KHttp.send({
     	           		url:"/sys_role/saveUserRole",
     	       		    data:{"userId":selRows[0].id,"roleIds":roleIds}}).done(function(res){
     	       		        top.$.messager.alert("提示", res.resMsg, "info"); 
     	       		    	top.$.modalDialog.handler.dialog('close');
	     	       	    });
        	    	}},{text:'取消',handler:function(){
        	    			top.$.modalDialog.handler.dialog('close');
        	    	}}]
     	        }).dialog('open');
 			});
		});
 		//添加实现
 		$("#add_user").click(function(){
 	        top.$.modalDialog({
                _form:"#userForm", 
                _datagrid:_this.userDataGrid,
 	            title : '用户信息添加',
 	            href:"/sys_user/updatePage",
 	            onLoad:function(){
 	            	var f=top.$.modalDialog.handler.find("#userForm"); 
 	            	f.form("reset");
 	            }
 	        }).dialog('open');
 		});
 		//删除实现
 		$("#del_user").click(function(){
 			var selRows=_this.userDataGrid.datagrid('getSelections');
 			JSmile.KEasy.del('/sys_user/deleteByIds',selRows,function(res){
 				_this.userDataGrid.datagrid('clearChecked').datagrid('reload');
 			});
 		});
 		$("#edit_user").click(function(){
 			var selRows=_this.userDataGrid.datagrid('getChecked');
 			JSmile.KEasy.edit(selRows,function(){
 	 	        top.$.modalDialog({
 	                _form:"#userForm", 
 	                _datagrid:_this.userDataGrid, 	 	        	
 	 	            title : '用户信息修改',
 	 	            href:"/sys_user/updatePage",
 	 	            onLoad:function(){
 	 	            	var f=top.$.modalDialog.handler.find("#userForm"); 
 	 	            	f.form("reset");
 	 	            	f.form("load",selRows[0]);
 	 	            	top.$('#username').textbox({'readonly':true}); 
 	 	            }
 	 	        }).dialog('open');
 			});
 		});
 		//搜索实现
 		$("#search_user").click(function(){
 			_this.userDataGrid.datagrid('load',$('#userSearchForm').serializeJson());
 		}); 
     },
     initDatagrid:function(){//初始化各种datagrid
    	var _this=this;
    	_this.userDataGrid=$('#user_datagrid').datagrid(JSmile.KEasy.initDatagrid({
	    	url:'/sys_user/findAllByPage',
	    	toolbar:'#user_toolbar',
	    	columns:[[
	   		 {field:'id',hidden:true},
			 {field:'ck',checkbox:true},
			 {field:'username',title:'用户名',align:'center',width:100},
			 {field:'name',title:'真实姓名',align:'center',width:100},
			 {field:'createDate',title:'注册时间',align:'center',width:100,sortable:true},
			 {field:'deptName',title:'部门',align:'center',width:100},
			 {field:'phone',title:'电话',align:'center',width:100},
			 {field:'email',title:'邮箱',align:'center',width:100},
			 {field:'birthday',title:'生日',align:'center',width:100,sortable:true},
			 {field:'geneder',title:'性别',align:'center',width:60,formatter:function(value,row,index){
				 if(2==value)
					 return "女";
				 else if(1==value)
					 return "男";
				 else
					 return "不详";
			 }},
			 {field:'status',title:'状态',align:'center',width:80,sortable:true,formatter:JSmile.KStatus.initStatus},
			 {field:'remark',title:'备注',align:'center',width:100},
			 {field:'action',title:'操作',align:'center',width:100,formatter: function(value,row,index){
				 if(row.status=='1'){
					 return JSmile.Kit.formatString('<a style="cursor:pointer;" class="easyui-tooltip" title=\"禁用\" onclick="updateUserStatus(\'{0}\',\'{1}\');" ><i style="color:#d9534f !important" class="iconfont sn-disable"></i></a>', row.id,0);
				 }else{
					 return JSmile.Kit.formatString('<a class="easyui-tooltip" style="cursor:pointer;" title=\"启用\" onclick="updateUserStatus(\'{0}\',\'{1}\');" ><i style="color:#5cb85c !important" class="iconfont sn-enable"></i></a>', row.id,1);
				 }
			 }}
		]]
	    }));
     }
  };
  user.init();
});
/**
 * 修改密码
 */
function updateUserStatus(id,status){
	JSmile.KHttp.send({
		url:"/sys_user/saveORupdate",
		data:{"id":id,"status":status}
	}).done(function(){
		$('#user_datagrid').datagrid('reload');
	});
}